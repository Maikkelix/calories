import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  genders = [];
  intensities = [];
  weight: number;
  gender: string;
  intensity: string;
  calories: number;

  constructor() {
    
   }

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    this.intensities.push('Light');
    this.intensities.push('Usual');
    this.intensities.push('Moderate');
    this.intensities.push('High');
    this.intensities.push('Very high');

    this.gender = 'Male';
    this.intensity = 'Usual';
  }

  calculate() {
    let factor = 0;

    switch (this.intensity) {
      case 'Light':
        factor = 1.3;
        break;
      case 'Usual':
        factor = 1.5;
        break;
      case 'Moderate':
        factor = 1.7;
        break;
      case 'High':
        factor = 2;
        break;
      case 'Very High':
        factor = 2.2;
        break;
    }

    if (this.gender === 'Male') {
      this.calories = (879 + 10.2 * this.weight) * factor;
    } else {
      this.calories = (759 + 7.18 * this.weight) * factor;
    }
  }
}
